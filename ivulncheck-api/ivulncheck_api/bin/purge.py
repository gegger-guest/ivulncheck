# Functions to purge data into database
#

from ivulncheck_api.lib.config_api import Configuration as conf

import logging
logging.basicConfig(filename=conf.getLogfile(), level=logging.INFO)

def purge_collections(db, base, collection):
    db.purgeCollection(base, collection)
    
def purge_all(db):
    db.purge_all()
