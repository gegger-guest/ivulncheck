import json, datetime, re

class Package(object):
    
    # Constructors
    def __init__(self, hosts, id, version, src_id, src_version, cpe_id, cpe_src_id, os_type, os_version):
        self.hosts = hosts
        self.id = id
        self.version = version
        self.src_id = src_id
        self.src_version = src_version
        self.cpe_id = cpe_id
        self.cpe_src_id = cpe_src_id
        self.os_type = os_type
        # SuiteName if Debian
        self.os_version = os_version

    # ~= toString(java)
    def __str__(self):
        string = "Hosts : "
        for host in self.hosts:
            string += host + "\n\t\t"
        string += "Package : " + self.id + "\n"
        string += "Version : " + self.version + "\n"
        string += "CPEID : " + self.cpe_id + "\n"
        string += "Source Package : " + self.src_id + "\n"
        string += "Source Version : " + self.src_verison + "\n"
        string += "Source CPEID : " + self.cpe_src_id + "\n"
        string += "OS Type : " + self.os_type + "\n"
        string += "OS Version : " + self.os_version + "\n"
        return string
        
    def to_json(self):
        data = {}
        data['hosts'] = self.hosts
        data['id'] = self.id
        data['version'] =  self.version
        data['cpe_id'] = self.cpe_id
        data['src_id'] = self.src_id
        data['src_version'] =  self.src_version
        data['cpe_src_id'] = self.cpe_src_id
        data['os_type'] = self.os_type
        data['os_version'] = self.os_version
        return data
        
class Product(object):
    
    # Constructors
    def __init__(self, hosts, id, version, cpe_id):
        self.hosts = hosts
        self.id = id
        self.version = version
        self.cpe_id = cpe_id

    # ~= toString(java)
    def __str__(self):
        string = "Hosts : "
        for host in self.hosts:
            string += host + "\n\t\t"
        string += "Product : " + self.id + "\n"
        string += "Version : " + self.verison + "\n"
        string += "CPEID : " + self.cpe_id + "\n"
        return string
        
    def to_json(self):
        data = {}
        data['hosts'] = self.hosts
        data['id'] = self.id
        data['version'] =  self.version
        data['cpe_id'] = self.cpe_id
        return data

class Vuln(object):
            
    # Constructors
    def __init__(self, id, src, bin_package, bin_versions, src_package, src_versions, vuln_versions, fixed, fixed_versions, remotely_exp, urgency, published_date):
        self.id = id
        self.src = src
        self.bin_package = bin_package
        self.bin_versions = bin_versions
        self.src_package = src_package
        self.src_versions = src_versions
        self.vuln_versions = vuln_versions
        self.fixed = fixed
        self.fixed_versions = fixed_versions
        self.remotely_exp = remotely_exp
        self.urgency = urgency
        self.published_date = published_date

        
    # ~= toString(java)
    def __str__(self):
        string = self.id + "\n"
        string += "Source : " + self.src + "\n"
        string += "Fixed : "
        for fixed_item in self.fixed:
            string += fixed_item + "\n\t"
        string += "Remotely Exploitable : "
        if self.remotely_exp is None:
            string += "unknown\n"
        elif self.remotely_exp == True:
            string += "yes\n"
        else:
            string += "no\n"
        string += "Urgency : " + self.urgency + "\n"
        string += "Binary Package : " + self.bin_package + "\n"
        string += "Binary Versions : "
        for bin_version in self.bin_versions:
            string += bin_version + "\n\t"
        string += "Source Package : " + self.src_package + "\n"
        string += "Source Versions : "
        for src_version in self.src_versions:
            string += src_version + "\n\t"
        string += "\nVulnerable Versions : "
        for vuln_version in self.vuln_versions:
            string += vuln_version + "\n\t"
        string += "\nFixed Versions : "
        for fixed_version in self.fixed_versions:
            string += fixed_version + "\n\t"
        string += "\nPublished Date : " + self.published_date
        return string
        
    def to_json(self):
        data = {}
        data['id'] = self.id
        data['src'] =  self.src
        data['bin_package'] = self.bin_package
        data['bin_versions'] = self.bin_versions
        data['src_package'] = self.src_package
        data['src_versions'] = self.src_versions
        data['vuln_versions'] = self.vuln_versions
        data['fixed'] = self.fixed
        data['fixed_versions'] = self.fixed_versions
        data['remotely_exp'] = self.remotely_exp
        data['urgency'] = self.urgency
        data['published_date'] = self.published_date
        return data

class Fix(object):
    
    # Constructors
    def __init__(self, hosts, bin_package, bin_version, src_package, src_version, fixed_version, remotely_exp, urgency, cve_ids, os_type, os_version, src, skipped, cve_published_date, fix_published_date):
        self.hosts = hosts
        self.bin_package = bin_package
        self.bin_version = bin_version
        self.src_package = src_package
        self.src_version = src_version
        self.fixed_version = fixed_version
        self.remotely_exp = remotely_exp
        self.urgency = urgency
        self.cve_ids = cve_ids
        self.os_type = os_type
        self.os_version = os_version
        self.src = src
        self.skipped = skipped
        self.cve_published_date = cve_published_date
        self.fix_published_date = fix_published_date
        
    # ~= toString(java)
    def __str__(self):
        string = "Hosts : "
        for host in self.hosts:
            string += host + "\n\t\t"
        string += "Binary Package : " + self.bin_package + "\n"
        string += "Binary version : " + self.bin_version + "\n"
        string += "Source Package : " + self.src_package + "\n"
        string += "Source Version : " + self.src_version + "\n"
        string += "Fixed Version : " + self.fixed_version + "\n"
        string += "Remotely Exploitable : "
        if self.remotely_exp is None:
            string += "unknown\n"
        elif self.remotely_exp == True:
            string += "yes\n"
        else:
            string += "no\n"
        string += "Urgency : " + self.urgency + "\n"
        string += "CVEIDs : "
        for cve_id in self.cve_ids:
            string += cve_id + "\n\t"
        string += "OS Type : " + self.os_type + "\n"
        string += "OS Version : " + self.os_version + "\n"
        string += "Source : " + self.src
        string += "Skipped : "
        if self.skipped == True:
            string += "yes\n"
        else:
            string += "no\n"
        string += "\nCVE Published Date : " + self.cve_published_date + "\n"
        string += "\nFix Published Date : " + self.fix_published_date + "\n"
        return string
        
    def to_json(self):
        data = {}
        data['hosts'] = self.hosts
        data['bin_package'] =  self.bin_package
        data['bin_version'] =  self.bin_version
        data['src_package'] =  self.src_package
        data['src_version'] =  self.src_version
        data['fixed_version'] =  self.fixed_version
        data['remotely_exp'] = self.remotely_exp
        data['urgency'] = self.urgency
        data['cve_ids'] =  self.cve_ids
        data['os_type'] =  self.os_type
        data['os_version'] =  self.os_version
        data['src'] =  self.src
        data['skipped'] = self.skipped
        data['cve_published_date'] = self.cve_published_date
        data['fix_published_date'] = self.fix_published_date
        return data
        
class Host(object):
    
    # Constructors
    def __init__(self, id, os_type, os_version, ipv4, last_modified):
        self.id = id
        self.os_type = os_type
        self.os_version = os_version
        #~ self.ipv6 = ipv6
        self.ipv4 = ipv4
        self.last_modified = last_modified

    # ~= toString(java)
    def __str__(self):
        string = self.id + "\n"
        string += "OS Type : " + self.os_type + "\n"
        string += "OS Version : " + self.os_version + "\n"
        #~ string += "IPv6 : " + self.ipv6 + "\n"
        string += "IPv4 : " + self.ipv4 + "\n"
        string += "Last Modified : " + self.lat_modified + "\n"
        return string
        
    def to_json(self):
        data = {}
        data['id'] = self.id
        data['os_type'] = self.os_type
        data['os_version'] = self.os_version
        #~ data['ipv6'] =  self.ipv6
        data['ipv4'] = self.ipv4
        data['last_modified'] = self.last_modified
        #~ data['pkgsvers'] = self.pkgsvers
        return data
                
