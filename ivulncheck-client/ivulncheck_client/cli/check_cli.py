# Script for cli interface to use check functions
#
# Made by Gaetan Egger
#
# Inspired from cve-search

# Imports
import argparse, sys, json

from ivulncheck_client.lib import ivulncheck_api_layer as ial

def main():
    # parse command line arguments
    argparser = argparse.ArgumentParser(description='Check vulnerabilities and fixes products in database')
    argparser.add_argument('-pkg2cpe', action='store_true', help='check if packages are referenced into CPE')
    argparser.add_argument('-debsecan_vulns', action='store_true', help='check vulnerabilities from debsecan')
    argparser.add_argument('-debsecan_fixes', action='store_true', help='check fixes from debsecan fixed tags')
    argparser.add_argument('-ubuntu_vulns', action='store_true', help='check vulnerabilities from ubuntu-cve-tracker')
    argparser.add_argument('-ubuntu_fixes', action='store_true', help='check fixes from ubuntu-cve-tracker fixed tags')
    argparser.add_argument('-product_vulns', action='store_true', help='check vulnerabilities for product from CVESearch')
    argparser.add_argument('-madison_fixes', action='store_true', help='check fixes from madison-lite')
    args = argparser.parse_args()
    
    
    if args.pkg2cpe:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/check/pkg2cpe")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.debsecan_vulns:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/check/debsecan_vulns")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.debsecan_fixes:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/check/debsecan_fixes")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.ubuntu_vulns:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/check/ubuntu_vulns")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.ubuntu_fixes:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/check/ubuntu_fixes")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.product_vulns:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/check/product_vulns")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.madison_fixes:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/check/madison_fixes")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    else:
        argparser.print_help()
        argparser.exit()
