# Script for cli interface to use check functions
#
# Made by Gaetan Egger
#
# Inspired from cve-search

# Imports
import argparse, sys, json

#~ from ivulncheck.lib.my_database_layer import DatabaseLayer
from ivulncheck_client.lib import ivulncheck_api_layer as ial


#~ from ivulncheck.bin import check

def main():
    # parse command line arguments
    argparser = argparse.ArgumentParser(description='Update/download fake repositories and ubuntu-cve-tracker tool')
    argparser.add_argument('-fakerepos', action='store_true', help='update/download fake repositories of debian-proposed-updates, debian-security, ubuntu-proposed, ubuntu-security for suites and releases in database')
    argparser.add_argument('-uct', action='store_true', help='update ubuntu-cve-tracker tool')

    
    args = argparser.parse_args()
    
    if args.fakerepos:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/fake_repos")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.uct:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/uct")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    else:
        argparser.print_help()
        argparser.exit()
