# Script for cli interface to use insert functions
#
# Made by Gaetan Egger
#
# Inspired from cve-search

# Imports
import argparse, sys, json

from ivulncheck_client.lib import ivulncheck_api_layer as ial

def main():
    # parse command line arguments
    argparser = argparse.ArgumentParser(description='Purge collection into database')
    argparser.add_argument('-pkg2cpe', action='store_true', help='purge pkg2cpe')
    argparser.add_argument('-hosts', action='store_true', help='purge hosts')
    argparser.add_argument('-host', type=str, help='purge the specified host')
    argparser.add_argument('-packages', action='store_true', help='purge packages')
    argparser.add_argument('-products', action='store_true', help='purge prodcuts')
    argparser.add_argument('-vulns', action='store_true', help='purge vuln')
    argparser.add_argument('-fixes', action='store_true', help='purge fix')
    argparser.add_argument('-all', action='store_true', help='purge all')
    
    args = argparser.parse_args()
    
    # Ask which product to add to database
    
    if args.pkg2cpe:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/purge/pkg2cpe")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.hosts:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/purge/hosts")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.host:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/purge/host/" + args.host)
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.packages:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/purge/packages")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.products:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/purge/products")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.vulns:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/purge/vulns")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.fixes:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/purge/fixes")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    elif args.all:
        response = ial.request_api('GET', "/ivulncheck_api/cmd/purge/all")
        sys.stdout.write(json.dumps({"Action" : response['action'], "Message" : response['message']}) + "\n")
        sys.stdout.flush()
    else:
        argparser.print_help()
        argparser.exit()
    
