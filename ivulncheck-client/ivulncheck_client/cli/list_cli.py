# Script for cli interface to use list functions
#
# Made by Gaetan Egger
#
# Inspired from cve-search

# Imports
import argparse, sys, json


from ivulncheck_client.lib import ivulncheck_api_layer as ial

def main():    
    # parse command line arguments
    argparser = argparse.ArgumentParser(description='Read Database')
    argparser.add_argument('-hosts', action='store_true', help='list hosts')
    argparser.add_argument('-packages', action='store_true', help='list packages')
    argparser.add_argument('-vulns', action='store_true', help='list vulnerabilities')
    argparser.add_argument('-fixes', action='store_true', help='list fixes')
    argparser.add_argument('-os_versions', action='store_true', help='list OS Versions')
    
    argparser.add_argument('-O', type=str, help='filters by OS Version')
    argparser.add_argument('-H', type=str, help='filters by host')
    argparser.add_argument('-P', type=str, help='filters by package name')
    argparser.add_argument('-U', type=str, help='filters by urgency (unknown|negligible|low|medium|high|critical)')
    argparser.add_argument('-R', type=str, help='filters by remotely exploitable criteria (unknown|yes|no)')
    argparser.add_argument('-CVE', type=str, help='filters by CVE ID')
    argparser.add_argument('-S', type=str, help='filters by source (debsecan|ubuntu|cve|madison)')
    argparser.add_argument('-F', type=str, help='filters by fixed tag (yes|no)')
    argparser.add_argument('-SP', type=str, help='filters by skipped tag (false|true)')
    
    argparser.add_argument('-l', default=0, type=int, help='limit')
    argparser.add_argument('-s', default=0, type=int, help='skip')
    
    argparser.add_argument('-r', action='store_true', help='to display in json raw format (whithout indentation)')
    
    args = argparser.parse_args()
    
    limit = args.l
    skip = args.s
    
    filters = {'cveSearchURL': "", 'pageLength': limit, 'r': skip, 'hostname': "all", 'cveid': "all", 'package': "all", 'urgency': "all", 'remotely_exp': "all", 'fixed': "all", 'os_version': "all", 'src': "all", 'skipped': False}
    
    if args.O:
        filters['os_version'] = args.O
    if args.H:
        filters['hostname'] = args.H
    if args.P:
        filters['package'] = args.P
    if args.U:
        filters['urgency'] = args.U
    if args.R:
        filters['remotely_exp'] = args.R
    if args.CVE:
        filters['cveid'] = args.CVE
    if args.S:
        filters['src'] = args.S
    if args.F:
        filters['fixed'] = args.F
    if args.SP:
        filters['skipped'] = args.SP
        
    
    if args.hosts:
        hosts = ial.request_api('POST', "/ivulncheck_api/hosts", filters)
        if args.r:
            sys.stdout.write(json.dumps(hosts))
        else:
            sys.stdout.write(json.dumps(hosts, indent=4) + "\n")
        sys.stdout.flush()
    elif args.packages:
        packages = ial.request_api('POST', "/ivulncheck_api/pkgs", filters)
        if args.r:
            sys.stdout.write(json.dumps(packages))
        else:
            sys.stdout.write(json.dumps(packages, indent=4) + "\n")
        sys.stdout.flush()
    elif args.vulns:
        vulns = ial.request_api('POST', "/ivulncheck_api/vulns", filters)
        if args.r:
            sys.stdout.write(json.dumps(vulns))
        else:
            sys.stdout.write(json.dumps(vulns, indent=4) + "\n")
        sys.stdout.flush()
    elif args.fixes:
        fixes = ial.request_api('POST', "/ivulncheck_api/fixes", filters)
        if args.r:
            sys.stdout.write(json.dumps(fixes, indent=4) + "\n")
        else:
            sys.stdout.write(json.dumps(fixes))
        sys.stdout.flush()
    elif args.os_versions:
        os_versions = ial.request_api('GET', "/ivulncheck_api/os_versions", filters)
        for os_version in os_versions:
            sys.stdout.write(os_version + " ")
            sys.stdout.flush()
    else:
        argparser.print_help()
        argparser.exit()
