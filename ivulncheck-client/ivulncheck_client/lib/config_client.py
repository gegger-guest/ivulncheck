import configparser

class DatabaseError(Exception):
    def __init__(self, message, status=502):
        self.message = message
        self.status  = status

class Configuration(): 
    ConfigParser = configparser.ConfigParser()
    # Open configuration file
    ConfigParser.read("/etc/ivulncheck/ivulncheck-client.ini")
    
    default = {'logging': True,           'logfile': "/var/log/ivulncheck/ivulncheck-client.log",
               'maxLogSize': '100MB',     'backlog': 5,
               'cvesearchAPIHost': "127.0.0.1",
               'cvesearchAPIPort': 5000,
               'ivulncheckAPIHost': "127.0.0.1",
               'ivulncheckAPIPort': 443,
               'ivulncheckAPIUsername': "ivulncheck_api",
               'ivulncheckAPIPassword': "api"
               }

    @classmethod
    def readSetting(cls, section, item, default):
        result = default
        try:
            if type(default) == bool:
                result = cls.ConfigParser.getboolean(section, item)
            elif type(default) == int:
                result = cls.ConfigParser.getint(section, item)
            else:
                result = cls.ConfigParser.get(section, item)
        except:
            pass
        return result

    # CVESearchAPI
    @classmethod
    def getCVESearchAPIHost(cls):
        return cls.readSetting("CVESearchAPI", "Host", cls.default['cvesearchAPIHost'])

    @classmethod
    def getCVESearchAPIPort(cls):
        return cls.readSetting("CVESearchAPI", "Port", cls.default['cvesearchAPIPort'])
    
    # IvulncheckAPI
    @classmethod
    def getIvulncheckAPIHost(cls):
        return cls.readSetting("IvulncheckAPI", "Host", cls.default['ivulncheckAPIHost'])

    @classmethod
    def getIvulncheckAPIPort(cls):
        return cls.readSetting("IvulncheckAPI", "Port", cls.default['ivulncheckAPIPort'])

    @classmethod
    def getIvulncheckAPIUsername(cls):
        return cls.readSetting("IvulncheckAPI", "Username", cls.default['ivulncheckAPIUsername'])
        
    @classmethod
    def getIvulncheckAPIPassword(cls):
        return cls.readSetting("IvulncheckAPI", "Password", cls.default['ivulncheckAPIPassword'])

    # Logging
    @classmethod
    def getLogfile(cls):
        return cls.readSetting("Logging", "Logfile", cls.default['logfile'])


class ConfigReader():
    def __init__(self, file):
        self.ConfigParser = configparser.ConfigParser()
        self.ConfigParser.read(file)

    def read(self, section, item, default):
        result = default
        try:
            if type(default) == bool:
                result = self.ConfigParser.getboolean(section, item)
            elif type(default) == int:
                result = self.ConfigParser.getint(section, item)
            else:
                result = self.ConfigParser.get(section, item)
        except:
            pass
        return result
