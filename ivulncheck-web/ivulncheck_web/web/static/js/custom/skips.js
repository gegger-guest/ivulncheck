function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function skip(pkg, vers) {
    alert("Skipping fix " + pkg + ":" + vers);
    $.getJSON("/ivulncheck_api/cmd/skip/" + pkg + ":" + vers);
    await sleep(100);
    window.location.reload();
}
async function unskip(pkg, vers) {
    alert("Unskipping fix " + pkg + ":" + vers);
    $.getJSON("/ivulncheck_api/cmd/unskip/" + pkg + ":" + vers);
    await sleep(100);
    window.location.reload();
}
