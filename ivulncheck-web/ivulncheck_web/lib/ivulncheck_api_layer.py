import http.client, json, urllib, datetime

from base64 import b64encode

try:
    from flask_wtf import FlaskForm
except ImportError:
    from flask_wtf import Form as FlaskForm

from ivulncheck_web.lib.config_web import Configuration as conf

import logging
logging.basicConfig(filename=conf.getLogfile(), level=logging.INFO)

IVULNCHECK_API_FQDN = conf.getIvulncheckAPIHost()
IVULNCHECK_API_PORT = conf.getIvulncheckAPIPort()
IVULNCHECK_API_USERNAME = conf.getIvulncheckAPIUsername()
IVULNCHECK_API_PASSWORD = conf.getIvulncheckAPIPassword()

class APIError(Exception):
    def __init__(self, message, status=500):
        self.message = message
        self.status  = status

def request_api(method, url_suffix, form=None):
    data = None
    # First try HTTPS
    try:
        conn = http.client.HTTPSConnection(IVULNCHECK_API_FQDN, IVULNCHECK_API_PORT)
        conn.connect()
    except Exception as e:
        conn = http.client.HTTPConnection(IVULNCHECK_API_FQDN, IVULNCHECK_API_PORT)
        logging.warning('[%s] (%s)\tAPI is not configured with SSL!.', datetime.datetime.today().isoformat(), __name__)
    try:
        userpass = b64encode((IVULNCHECK_API_USERNAME + ":" + IVULNCHECK_API_PASSWORD).encode("ascii")).decode("ascii")
        
        if form and type(form) is dict:
            logging.info('%s', form)
            body=urllib.parse.urlencode(form)
            headers = {"Authorization" : "Basic " + userpass, "Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
            conn.request(method, url_suffix, body, headers)
        elif form and issubclass(type(form), FlaskForm):
            # To convert FlaskForm to dict
            form_data = form.data
            body=urllib.parse.urlencode(form_data)
            headers = {"Authorization" : "Basic " + userpass, "Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
            conn.request(method, url_suffix, body, headers)
        else:
            headers = {"Authorization" : "Basic " + userpass}
            conn.request(method, url_suffix, headers=headers)
        
        data = json.loads(conn.getresponse().read().decode("utf-8"))
    except TypeError:
        data = conn.getresponse().read().decode("utf-8")
    except Exception as e:
        raise(APIError("Ivulncheck API unreachable : " + str(e)))
    finally:
        conn.close()
        if type(data) is dict  and 'error' in data.keys():
            raise(APIError("APIError : " + data['message']))
    return data
