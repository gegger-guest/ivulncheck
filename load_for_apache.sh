#!/bin/bash
cd ivulncheck-api
python3 setup.py build
python3 setup.py install

cd ../ivulncheck-web
python3 setup.py build
python3 setup.py install

cp -r /usr/local/lib/python3.5/dist-packages/ivulncheck_api/* /usr/share/ivulncheck-api/
cp -r /usr/local/lib/python3.5/dist-packages/ivulncheck_web/* /usr/share/ivulncheck-web/



