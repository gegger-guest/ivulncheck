Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ivulncheck
Upstream-Contact: Infomaniak Network <production@infomaniak.com>
Source: https://salsa.debian.org/gegger-guest/ivulncheck

Files: *
Copyright: Infomaniak Network <production@infomaniak.com>
License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
    http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian-based systems the full text of the Apache version 2.0 license
 can be found in /usr/share/common-licenses/Apache-2.0.

Files:
 ivulncheck-api/ivulncheck_api/api/api.py
 ivulncheck-api/ivulncheck_api/api/templates/*
 ivulncheck-api/ivulncheck_api/lib/database_layer.py
 ivulncheck-api/ivulncheck_api/lib/config_api.py
 ivulncheck-web/ivulncheck_web/lib/config_web.py
 ivulncheck-web/ivulncheck_web/web/index.py
 ivulncheck-web/ivulncheck_web/web/static/css/sidebar.css
 ivulncheck-web/ivulncheck_web/web/static/css/style.css
 ivulncheck-web/ivulncheck_web/web/templates/* 
Copyright: (c) 2013-2016, Alexandre Dulaunoy
           (c) 2014-2016, Pieter-Jan Moreels <pieterjan.moreels@gmail.com>
Comment: These files are inspired from CVE-Search project, many modifications
 were done but based on the original.
License: BSD-3-Clause

Files: ivulncheck-api/ivulncheck_api/lib/desecan_layer.py
Copyright: (c) 2005-2006, Florian Weimer
Comment:
 This lib file uses source code of debsecan to search for vulnerabilities
 concerning a list of packages stored in a MongoDB database.
License: GPL-2

Files:
 ivulncheck-api/ivulncheck_api/lib/ubuntu_cve_tracker_layer.py
 ivulncheck-api/ivulncheck_api/lib/uct/*
Copyright:
 2005-2018, Ubuntu Securitiy Team
 2004-2018, Canonical Ltd.
License: GPL-2

Files:
 ivulncheck-web/ivulncheck_web/web/static/css/vendors/bootstrap.css
 ivulncheck-web/ivulncheck_web/web/static/js/vendors/bootstrap.js
Copyright:
 2011-2015, Twitter, Inc
License: Expat

Files: ivulncheck-web/ivulncheck_web/web/static/js/vendors/d3.js
Copyright: (c) 2010-2017, Mike Bostock
License: BSD-3-Clause

Files: ivulncheck-web/ivulncheck_web/web/static/js/vendors/jquery.js
Copyright: (c) 2016, jQuery Foundation and other contributors
License: Expat

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
  * Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 .
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
  * Neither the name of the author nor the names of contributors may be used to
    endorse or promote products derived from this software without specific prior
    written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: GPL-2
 This is free software; you can redistribute it and/or modify it under the
 terms of version 2 of the GNU General Public License version 2 as published
 by the Free Software Foundation.
 .
 This is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public License along with
 this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems the full copy of the GPL-2 License can be found at:
 /usr/share/common-licenses/GPL-2

