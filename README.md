Ivulncheck is a security tool to centralize the search of vulnerabilities and
 fixes concerning packages on a cluster.
 .
 A procedure is needed to initialize the API (restore an initial state of the
 MongoDB database, download ubuntu-cve-tracker tool, deploy CVE-Search modified
 API)
 .
 First, an agent script must be deployed to detect installed packages on the
 cluster (only Ubuntu and Debian are supported). This script generated a json
 formatted file and send it to ther API.
 Then, several commands must be executed to search vulnerabilities and
 determine available security updates (called here fixes). Four principals
 sources are used :
 .
  * debsecan for Debian hosts,
  * ubuntu-cve-tracker for Ubuntu hosts,
  * CVE-Search for custom installed application (manually configuration
    needed),
  * madison-lite (to complete missing fixes due to possibly outdated sources,
    missing sources or unreferenced vulnerabilities)
 .
 Eventually, you can get informations (installed packages, monitored hosts,
 known vulnerabilities an available fixes) directly from the API
 Or a Web Interface provide a graphical rendering of these informations.

TODO
* set a user with restricted permissions for ivulncheck-agent package
* change Debian version detection fron lsb_release command to /etc/debian_version parsing
* switch MongoDB to MariaDB
